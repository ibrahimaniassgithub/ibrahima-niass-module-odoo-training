from odoo import models, fields, api
from datetime import date


class student(models.Model):
    _inherit = 'res.partner'
    age = fields.Integer(string="Age")
    birth_date = fields.Date(string="Date de naissance")
    majeur = fields.Boolean(string="Majeur",default=True)
    sexe = fields.Selection([('H','Homme'),('F','Femme')], 'sexe', default='H')

    _sql_constraints = [
        ('_majeur_true',
         'check(majeur)', "L'etudiant doit étre majeur ...!")]

    @api.onchange('birth_date')
    def calcul_age(self):
        if self.birth_date and type(self.birth_date)!='bool':
             _logger.error(type(self.birth_date))
             today = date.today()
             self.age = today.year - self.birth_date.year - ((today.month, today.day) < (self.birth_date.month, self.birth_date.day))

class teacher(models.Model):
    _inherit = 'res.partner'
    grade =fields.Char(string='Grade')
    sexe = fields.Selection([('H','Homme'),('F','Femme')], 'sexe', default='H')


